<?php

namespace QI\NFS\App\Engine;

class FourthCar implements EngineInterface
{

    public function getPower(): int
    {
        return 300;
    }

    public function getFuelConsumption(): int
    {
        return 13;
    }
}
