<?php
namespace QI\NFS\App\Engine;

class SecondCar implements EngineInterface
{

    public function getPower():int
    {
        return 200;
    }
    public function getFuelConsumption(): int
    {
        return 10;
    }
}
