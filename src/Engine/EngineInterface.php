<?php
namespace QI\NFS\App\Engine;
interface EngineInterface{
    public function getPower():int;
    public function getFuelConsumption():int;
}