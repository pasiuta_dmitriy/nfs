<?php

namespace QI\NFS\App\Body;

interface BodyInterface{
    public function getMass():int;
    public function getVolume():int;
}