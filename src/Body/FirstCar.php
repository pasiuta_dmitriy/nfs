<?php

namespace QI\NFS\App\Body;

class FirstCar implements BodyInterface
{
    public function getMass():int
    {
      return 2000;
    }
    public function getVolume(): int
    {
        return 150;
    }
}