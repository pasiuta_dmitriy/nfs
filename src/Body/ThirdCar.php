<?php


namespace QI\NFS\App\Body;

class ThirdCar implements BodyInterface
{
    public function getMass(): int
    {
        return 1500;
    }

    public function getVolume(): int
    {
        return 100;
    }
}