<?php
namespace QI\NFS\App;



use QI\NFS\App\Body\BodyInterface;
use QI\NFS\App\Engine\EngineInterface;

class Car
{

    /**
     * @var BodyInterface
     */
    private $body;
    /**
     * @var EngineInterface
     */
    private $engine;

    function __construct(BodyInterface $body, EngineInterface $engine) {

        $this->body = $body;
        $this->engine = $engine;
    }
    public function getSpeed(){
     return $this->body->getMass() / $this->engine->getPower();
     // $this->engine->getPower();

    }
    public function getRange(){
       return $this->body->getVolume() *  $this->engine->getFuelConsumption();

    }
}



