<?php

use QI\NFS\App\Body\BodyInterface;

include __DIR__ . "/vendor/autoload.php";

$bodies = [
    new \QI\NFS\App\Body\FirstCar(),
    new \QI\NFS\App\Body\ThirdCar()
];

$engines = [
    new \QI\NFS\App\Engine\SecondCar(),
    new \QI\NFS\App\Engine\FourthCar()
];
$body = $engine = null;


echo "Сhoose your desired Body: \n";
foreach ($bodies as $key => $value) {
    $key++;
    echo "$key) mass: {$value->getMass()}, volume: {$value->getVolume()} \n";

}
while ($bodyInput = readline()) {

    if (!isset($bodies[(int)$bodyInput - 1])) {
        echo "Please type a number from list:  ";
        continue;
    }
    $body = $bodies[$bodyInput - 1];
    break;
}
echo "Choose your desired Engine: \n";
foreach ($engines as $key => $value) {
    $key++;
    echo "$key) mass: {$value->getPower()}, volume: {$value->getFuelConsumption()} \n";
}
while ($engineInput = readline()) {

    if (!isset($engines[(int)$engineInput - 1])) {
        echo "Please type a number from list:  ";
        continue;
    }
    $engine = $engines[$engineInput - 1];
    break;
    //BodyInterface::class

};
$car = new \QI\NFS\App\Car($body, $engine);
echo $car->getRange();
echo "\n";
echo $car->getSpeed();
echo "\n";




